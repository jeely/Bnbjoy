﻿using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Concrete
{
    public class PermissionRepository : IPermissionRepository
    {
        public async Task<bool> Insert(Permission permission)
        {
            using (BnbjoyBackendDbEntities bnbjoyBackendDbEntities = new BnbjoyBackendDbEntities())
            {
                var existedPermission = await bnbjoyBackendDbEntities.Permissions.AsNoTracking().Where(r => r.PermissionTitle == permission.PermissionTitle).FirstOrDefaultAsync();
                if (existedPermission == null)
                {
                    bnbjoyBackendDbEntities.Permissions.Add(permission);
                    return await bnbjoyBackendDbEntities.SaveChangesAsync() > 0;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}

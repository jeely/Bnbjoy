﻿using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Concrete
{
    public class RoleRepository : IRoleRepository
    {

        public async Task<bool> Insert(Role role)
        {
            try
            {
                using (BnbjoyBackendDbEntities bnbjoyBackendDbEntities = new BnbjoyBackendDbEntities())
                {
                    var existedRole = await bnbjoyBackendDbEntities.Roles.AsNoTracking().Where(r => r.RoleName == role.RoleName).FirstOrDefaultAsync();
                    if (existedRole == null)
                    {
                        bnbjoyBackendDbEntities.Roles.Add(role);
                        return await bnbjoyBackendDbEntities.SaveChangesAsync() > 0;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex) 
            {
                return false;
            }

        }

    }
}

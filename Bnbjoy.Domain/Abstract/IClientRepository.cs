﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Abstract
{
    public interface IClientRepository
    {
        Task<Client> FindById(string Id);

        Task<Client> FirstClient();

        Task<Client> InsertClient(Client client);

        Task<IEnumerable<Client>> InsertClients(IEnumerable<Client> clients);
    }
}

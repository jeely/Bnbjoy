﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Abstract
{
    public interface IMobileCaptchaRepository
    {
        Task<MobileCaptcha> FindByNumber(string num);

        Task<bool> Insert(MobileCaptcha mobileCaptcha);

        Task<bool> Delete(MobileCaptcha mobileCaptcha);

    }
}

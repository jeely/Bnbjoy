﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Abstract
{
    public interface IUserRepository
    {
        Task<bool> Insert(User user, Profile profile);

        Task<User> Find(string userName, string password);

        Task<User> Retreive(string userName);

        Task<User> RetreiveByEmail(string email);

        Task<User> FindByMobileNumber(string mobileNumber);

        Task<dynamic> DashUserInfo(string userName);

        Task<dynamic> UserAndProfileInfo(string userName);

        Task<IEnumerable<dynamic>> RetreiveEmployeesInfo(string ownerId);

        Task<IEnumerable<dynamic>> RetreiveBA_Bnbs(string userName);

        Task<IEnumerable<dynamic>> RetreiveBE_Bnbs(string userName);

        Task<dynamic> RetreiveUserAndPermission(string userId, string role, string bnbId);

        Task<bool> UpdatePermission(string userId, string bnbId, string permissionStr);

        Task<IEnumerable<dynamic>> BnbPermissionList(string userId);

        Task<IEnumerable<dynamic>> InsertDeleteBnbPermission(string userId, List<Tuple<string, bool>> list);
    }
}

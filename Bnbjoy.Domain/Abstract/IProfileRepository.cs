﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Abstract
{
    public interface IProfileRepository
    {
        Task<bool> Insert(Profile profile);
    }
}

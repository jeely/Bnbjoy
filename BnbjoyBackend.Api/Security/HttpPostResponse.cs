﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace BnbjoyBackend.Api.Security
{
    public class HttpPostResponse
    {
        public HttpPostResponse() { }

        public HttpPostResponse(HttpStatusCode code, string content) 
        {
            this.StatusCode = code;
            this.Content = content;
        }

        public HttpStatusCode StatusCode { get; set; }
        public string Content { get; set; }
    }
}
﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var response = GetToken();
            if (response.IsError && response.Error.Equals("invalid_client", StringComparison.CurrentCultureIgnoreCase))
            {
                Console.WriteLine("无效的用户，跳转至登录页...");
                return;
            }
            CallService(response.AccessToken);
            
            Console.WriteLine("response.AccessToken:" + response.AccessToken);
            Console.WriteLine("response.Json:" + response.Json);
            Console.WriteLine("response.Raw:" + response.Raw);
            Console.WriteLine("response.RefreshToken:" + response.RefreshToken);
            Console.WriteLine("response.TokenType:" + response.TokenType);


            JObject resObj = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Raw);
            var value = resObj["access_token"];
            var refreshResponse = RefreshToken(response.RefreshToken);
            if (refreshResponse.IsError && refreshResponse.Error.Equals("invalid_client", StringComparison.CurrentCultureIgnoreCase))
            {
                Console.WriteLine("无效的用户，跳转至登录页...");
                return;
            }
            CallService(refreshResponse.AccessToken);
        }
    }
}

﻿using Bnbjoy.Business.Abstract;
using Bnbjoy.Business.Common;
using Bnbjoy.Business.Concrete;
using Bnbjoy.Business.Constants;
using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Concrete;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            //string orderId = BusinessUtil.GenOrderId(OrderType.BO);
            //int orderStatusVal = CommonUtil.ObtainValueFromEnumStr(typeof(OrderStatus), "Processing");

            using (IRoomTypeService roomtypeService = new RoomTypeService(new RoomTypeRepository()))
            {
                DateTime fromDate = DateTime.Parse("2016-11-01");
                DateTime toDate = DateTime.Parse("2016-11-30");
                var results = roomtypeService.FetchAllBasicRoomsInfo(fromDate, toDate, "2");
                Console.WriteLine(results);
            }

            //DateTime fromDate = DateTime.Parse("2016-11-01");
            //DateTime toDate = DateTime.Parse("2016-11-30");
            //IRoomTypeRepository roomTypeRepository = new RoomTypeRepository();
            //roomTypeRepository.ListBasicRoomsInfo(fromDate, toDate, "2");

            //Stopwatch watch = new Stopwatch();
            //watch.Start();
            //bool result = false;

            //IRoomTypeRepository roomTypeRepository = new RoomTypeRepository();
            ////假设数据库存在范围：2016-11-12 00:00:00 -- 2016-11-17 00:00:00

            ////模拟第4种情况
            ////roomTypeRepository.UpdateDailyPrice("2016-11-14", "2016-11-15", "8d97b17cf66e43d09ce25c6e50b5eb62", 140);
            
            ////模拟第1种情况
            ////roomTypeRepository.UpdateDailyPrice("2016-11-14", "2016-11-19", "8d97b17cf66e43d09ce25c6e50b5eb62", 110);

            ////模拟第3种情况
            ////roomTypeRepository.UpdateDailyPrice("2016-11-09", "2016-11-14", "8d97b17cf66e43d09ce25c6e50b5eb62", 130);

            ////假设数据库存在范围：2016-11-12 00:00:00 -- 2016-11-17 00:00:00, 2016-11-20 00:00:00 -- 2016-11-25 00:00:00
            ////
            ////roomTypeRepository.UpdateDailyPrice("2016-11-15", "2016-11-23", "8d97b17cf66e43d09ce25c6e50b5eb62", 120);

            ////IUserRepository userRepository = new UserRepository();
            ////dynamic result = userRepository.DashUserInfo("jizhuang");
            ////Console.WriteLine(JsonConvert.SerializeObject(result));
            //watch.Stop();
            //Console.WriteLine(watch.ElapsedMilliseconds.ToString());
            //Console.Read();
            MainAsync().Wait();
        }

        static async Task MainAsync() 
        {
            bool result = false;
            IRoomTypeRepository roomTypeRepository = new RoomTypeRepository();
            //result = await roomTypeRepository.UpdateDailyPrice("2016-11-09", "2016-11-12", "8d97b17cf66e43d09ce25c6e50b5eb62", 120);

            //result = await roomTypeRepository.UpdateDailyPrice("2016-11-17", "2016-11-19", "8d97b17cf66e43d09ce25c6e50b5eb62", 120);

            //result = await roomTypeRepository.UpdateDailyPrice("2016-11-15", "2016-11-23", "8d97b17cf66e43d09ce25c6e50b5eb62", 160);

            //result = await roomTypeRepository.UpdateDailyPrice("2016-11-09", "2016-11-14", "8d97b17cf66e43d09ce25c6e50b5eb62", 130);

            //result = await roomTypeRepository.UpdateDailyPrice("2016-11-14", "2016-11-19", "8d97b17cf66e43d09ce25c6e50b5eb62", 110);

            //result = await roomTypeRepository.UpdateDailyPrice("2016-11-09", "2016-11-19", "8d97b17cf66e43d09ce25c6e50b5eb62", 80);

            //result = await roomTypeRepository.UpdateDailyPrice("2016-11-09", "2016-11-09", "8d97b17cf66e43d09ce25c6e50b5eb62", 80);
            Console.WriteLine(result); 

            //var clientCredential_TokenRes = TokenManager.Instance.GetPublicAccessToken();

            ////短信验证码服务
            ////dynamic dynamicJson = new ExpandoObject();
            ////dynamicJson.mobileNumber = "13918928709";
            ////string jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            ////HttpPostResponse serviceResponse = await ServiceManager.Instance.PostService(clientCredential_TokenRes.AccessToken, "Captcha/Mobile", jsonObj);
            ////if (serviceResponse.StatusCode == HttpStatusCode.BadRequest)
            ////{
            ////    Console.WriteLine(serviceResponse.Content);
            ////}

            ////dynamic registerJson = new ExpandoObject();
            ////registerJson.roleName = "BA";
            ////registerJson.mobileNumber = "13918928709";
            ////registerJson.mobileCaptcha = "111111"; //之前获取的验证码，此处为mock
            ////registerJson.userName = "zhengheng";
            ////registerJson.password = "Test#123";
            ////registerJson.email = "zhengheng@163.com";
            ////string registerObj = JsonConvert.SerializeObject(registerJson);
            ////HttpPostResponse registerResponse = await ServiceManager.Instance.PostService(clientCredential_TokenRes.AccessToken, "Account/Register", registerObj);
            ////if (registerResponse.StatusCode == HttpStatusCode.BadRequest)
            ////{
            ////    Console.WriteLine(registerResponse.Content);
            ////}

            //var passwordCredetial_TokenResponse = TokenManager.Instance.GetToken("zhengheng", "Test#123");
            //if (passwordCredetial_TokenResponse.IsError && passwordCredetial_TokenResponse.Error.Equals("invalid_client", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    Console.WriteLine("无效的用户，跳转至登录页...");
            //    return;
            //}

            //Console.WriteLine("response.AccessToken:" + passwordCredetial_TokenResponse.AccessToken);
            //Console.WriteLine("response.Json:" + passwordCredetial_TokenResponse.Json);
            //Console.WriteLine("response.Raw:" + passwordCredetial_TokenResponse.Raw);
            //Console.WriteLine("response.RefreshToken:" + passwordCredetial_TokenResponse.RefreshToken);
            //Console.WriteLine("response.TokenType:" + passwordCredetial_TokenResponse.TokenType);

            //var getValues = await ServiceManager.Instance.GetService(passwordCredetial_TokenResponse.AccessToken, "Values");
            //Console.WriteLine(getValues);

            //var refreshResponse = TokenManager.Instance.RefreshToken(passwordCredetial_TokenResponse.RefreshToken);
            //if (refreshResponse.IsError && refreshResponse.Error.Equals("invalid_client", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    Console.WriteLine("无效的用户，跳转至登录页...");
            //    return;
            //}

            //getValues = await ServiceManager.Instance.GetService(refreshResponse.AccessToken, "Values");
            //Console.WriteLine(getValues);

            Console.ReadLine();
        }

    }
}

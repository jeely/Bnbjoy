﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BnbjoyBackend.Site
{
    public static class Common
    {
        public static bool IsPropertyExist(dynamic obj, string name)
        {
            return obj.GetType().GetProperty(name) != null;
        }

    }
}
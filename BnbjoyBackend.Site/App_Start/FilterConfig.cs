﻿using System.Web;
using System.Web.Mvc;

namespace BnbjoyBackend.Site
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

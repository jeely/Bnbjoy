﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace BnbjoyBackend.Site.Extends
{
    public static class ExtensionMethods
    {
        public static string ToEnumString(this HttpStatusCode statusCode)
        {
            return Enum.GetName(typeof(HttpStatusCode), statusCode);
        }

        public static string GetBaseUrl(this HttpRequestBase request)
        {
            if (request.Url == (Uri)null)
                return string.Empty;
            else
                return request.Url.Scheme + "://" + request.Url.Authority + VirtualPathUtility.ToAbsolute("~/");
        }
    }
}
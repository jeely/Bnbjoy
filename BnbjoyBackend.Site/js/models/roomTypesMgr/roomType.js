/**
 * Created by lijizhuang on 16/9/19.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var roomType = Backbone.Model.extend({
        defaults:{
            roomTypeId:'',  //房型id
            roomTypeName:'', //房型名
            roomTypeNotes:'', //房型备注
            bnbId:'',  //民宿id
            rank: '',  //房型排名id
            sort:'', //当前房型顺序
            roomModels:[],  //房间集合
            defaultPrice:'' //价格信息
        },
        
        initialize: function () {
            
        }
    });
    
    return roomType;
});
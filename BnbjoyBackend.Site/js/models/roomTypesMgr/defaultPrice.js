/**
 * Created by lijizhuang on 16/9/19.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var defaultPrice = Backbone.Model.extend({
        defaults: {
            roomTypeId: '',
            //uniformPrice: false, //统一价格,如果为true,每天都使用uniformPrice
            initPrice: 0,
            mondayPrice: 0,      //周一至周末每天的价格
            tuesdayPrice: 0,
            wednesdayPrice: 0,
            thursdayPrice: 0,
            fridayPrice: 0,
            saturdayPrice: 0,
            sundayPrice: 0
        },

        initialize: function () {

        }
    });

    return defaultPrice;
});
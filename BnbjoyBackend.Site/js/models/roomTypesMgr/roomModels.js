/**
 * Created by lijizhuang on 16/9/19.
 */
define(['jquery', 'underscore', 'backbone', 'models/roomTypesMgr/roomModel'], function ($, _, Backbone, RoomModel) {
    var roomModels = Backbone.Collection.extend({
        model: RoomModel,
        initialize: function () {

        }
    });

    return roomModels;
});
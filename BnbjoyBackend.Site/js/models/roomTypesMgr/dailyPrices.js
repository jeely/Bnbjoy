/**
 * Created by lijizhuang on 16/9/22.
 */
define(['jquery', 'underscore', 'backbone', 'models/roomTypesMgr/dailyPrice', 'extensions/dateExts', 'common', 'url'],
    function ($, _, Backbone, DailyPrice, DateExts, Common, Url) {
    var dailyPrices = Backbone.Collection.extend({
        model: DailyPrice,
        initialize: function () {
            
        },

        //模拟月价格获取, 参数yyyy-MM
        syncDailyPrice: function (date, roomTypeId, successCB, failCB) {
            var fromDate = new Date(date.replace(/-/g, "/"));

            var year = fromDate.getFullYear();
            var month = fromDate.getMonth() + 1;

            var days ;

            //当月份为二月时，根据闰年还是非闰年判断天数
            if(month == 2){
                days= year % 4 == 0 ? 29 : 28;

            }
            else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12){
                //月份为：1,3,5,7,8,10,12 时，为大月.则天数为31；
                days= 31;
            }
            else{
                //其他月份，天数为：30.
                days= 30;
            }

            var postModel = {};
            postModel.year = year;
            postModel.month = month;
            postModel.roomTypeId = roomTypeId;

            Common.sendAjaxRequest(Url.listDailyPrice, postModel, _.bind(function (data) {
                if (data.Content && data.Content.length > 0) {
                    var prices = JSON.parse(data.Content);
                    
                    for (var i = 1; i <= days; i++) {
                        var dailyPrice = new DailyPrice();
                        var tempDate = new Date((year + '-' + month + '-' + i).replace(/-/g, "/")).yyyyMMdd(); //拼接日期,转yyyy-MM-dd

                        var date_dd = new Date(tempDate).getDate();
                        var festival = Common.festivalCheck(month, i, date_dd);
                        if (date_dd != festival) {
                            dailyPrice.set({
                                festival: festival
                            });
                        }

                        var canChangePrice = new Date(tempDate) > new Date(); //如果是将来的时间,允许改动价格

                        dailyPrice.set({
                            date: tempDate,  //接口返回的日期, 'yyyy-MM-dd'
                            date_dd: date_dd,  //得到该月的第几天
                            price: prices[i - 1],    //接口返回的价格
                            dayOfWeek: new Date(tempDate).getDay(),  //得到星期几
                            canChangePrice: canChangePrice  //是否可以改价格
                        });
                        this.add(dailyPrice);
                    }

                    successCB && (typeof successCB === 'function') && successCB(this);
                }
            }, this), _.bind(function (data) {
                if (data.Content && data.Content.length > 0) {
                    failCB && (typeof failCB === 'function') && failCB(data);
                }
            }, this), true, true, true);
        }

    });

        return dailyPrices;
});
/**
 * Created by lijizhuang on 16/9/1.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var LoginInfo = Backbone.Model.extend({
        defaults: {
            userName : '',
            password : '',
            store30Days: ''
        },

        validate: function(attributes, option){
            if($.trim(attributes.userName).length === 0){
                return '用户名不能为空';
            }

            if($.trim(attributes.password).length === 0){
                return '密码不能为空';
            }
        }
    });

    return LoginInfo;
});
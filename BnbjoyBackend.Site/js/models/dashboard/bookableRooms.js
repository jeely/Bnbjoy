/**
 * Created by lijizhuang on 16/9/7.
 */
define(['jquery', 'underscore', 'backbone', 'models/dashboard/bookableRoom'], function ($, _, Backbone, BookableRoom) {
    var bookableRooms = Backbone.Collection.extend({
        model:BookableRoom,
        initialize: function () {
            
        },

        requestAsync: function (fromDateStr, callback) {
            this.reset();

            //ajax请求获取特定时间段的可预定房间
            //mock接口
            var fromDate = new Date(fromDateStr);
            for(var i=0; i<30; i++){
                var currentDate = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate()+i);

                var bookableRoom = new BookableRoom();
                bookableRoom.set({date:currentDate, totalRooms:4, availableRooms:3});
                this.add(bookableRoom);
            }
        }

    });

    return bookableRooms;
});
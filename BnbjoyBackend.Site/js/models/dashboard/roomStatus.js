/**
 * Created by lijizhuang on 16/9/9.
 */
define([], function () {
    var RoomStatus = {
        Away: 'Away',                //已离店
        Bookable: 'Bookable',       //可预定
        Booked: 'Booked',          //已预定
        CheckedIn: 'CheckedIn',  //已入住
        Makeup: 'Makeup',       //过期未预定(可补录)
        Locked: 'Locked'       //关房
    };

    return RoomStatus;
});
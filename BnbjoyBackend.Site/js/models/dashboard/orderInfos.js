/**
 * Created by lijizhuang on 16/9/8.
 */
define(['jquery', 'underscore', 'backbone', 'models/dashboard/orderInfo'], function ($, _, Backbone, OrderInfo) {
    var orderInfos = Backbone.Collection.extend({
        model: OrderInfo,
        initialize: function () {

        }
    });

    return orderInfos;
});
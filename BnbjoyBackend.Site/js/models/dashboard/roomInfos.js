/**
 * Created by lijizhuang on 16/9/8.
 */
define(['jquery', 'underscore', 'backbone', 'models/dashboard/roomInfo', 'models/dashboard/orderInfo', 'models/dashboard/orderInfos',
        'models/dashboard/roomStatus','models/dashboard/paymentType', 'models/dashboard/channelType'],
  function ($, _, Backbone, RoomInfo, OrderInfo, OrderInfos, RoomStatus, PaymentType, ChannelType) {
      var roomInfos = Backbone.Collection.extend({
          model: RoomInfo,
          initialize: function () {

          },

          requestAsync: function (fromDateStr, callback) {
              this.reset();

              //mock ajax请求该时间段房间信息
              var fromDate = new Date(fromDateStr);

              //第1个房间
              var roomInfo = new RoomInfo();
              roomInfo.set({ roomId: "ca02df3ce243471f873e1db2860aded5", roomNumber: '210', roomTypeId: "d946f9ed466a4adab3a7b854f5a69025", roomType: '春华秋实房' });

              //获取订单信息
              var orderInfo = new OrderInfo();
              orderInfo.set({
                             orderId:11946402,
                             fromDate: "2016-09-25 00:00:00",
                             toDate:"2016-09-26 00:00:00",
                             fromRNightDate:"2016-11-25 00:00:00",
                             toRNightDate:"2016-11-25 00:00:00",
                             roomId: 864959,
                             roomStatus: RoomStatus.CheckedIn,
                             paymentType: PaymentType.PIC,
                             arrivalTime: "18:00",
                             roomNights: 1,
                             reserverName: "方大同",
                             reserverMobile: "13696967251",
                             mobileArea: "厦门",
                             amount: 250.00,
                             channel: ChannelType.Offline,
                            }); 

              var orderInfos = new OrderInfos();
              orderInfos.add(orderInfo);
              roomInfo.set({orders: orderInfos});

              var prices = ["250.0000", "200.0000", "200.0000", "200.0000", "200.0000", "250.0000", "250.0000", "250.0000", "200.0000", "200.0000", "200.0000", "200.0000", "250.0000", "250.0000", "250.0000", "200.0000", "200.0000", "200.0000", "200.0000", "250.0000", "250.0000", "250.0000", "200.0000", "200.0000", "200.0000", "200.0000", "250.0000", "250.0000", "250.0000", "200.0000"];
              roomInfo.set({dailyPrices: prices});

              this.add(roomInfo);

              //第2个房间
              var roomInfo1 = new RoomInfo();
              roomInfo1.set({ roomId: "ef074f2f5cd04fa1954718c8bcef5c32", roomNumber: '222', roomTypeId: "d946f9ed466a4adab3a7b854f5a69025", roomType: '春华秋实房' });

              var orderInfo1 = new OrderInfo();
              orderInfo1.set({
                  orderId:11937542,
                  fromDate: "2016-09-25 00:00:00",
                  toDate: "2016-09-28 00:00:00",
                  fromRNightDate: "2016-09-25 00:00:00",
                  toRNightDate:"2016-09-27 00:00:00",
                  roomId: 867899,
                  roomStatus: RoomStatus.CheckedIn,
                  paymentType: PaymentType.PIC,
                  arrivalTime: "18:00",
                  roomNights: 1,
                  reserverName: "牛群",
                  reserverMobile: "13696967251",
                  mobileArea: "厦门",
                  amount: 250.00,
                  channel: ChannelType.Offline,
              });

              var orderInfos1 = new OrderInfos();
              orderInfos1.add(orderInfo1);
              roomInfo1.set({orders: orderInfos1});

              var prices1 = ["250.0000", "200.0000", "200.0000", "200.0000", "200.0000", "250.0000", "250.0000", "250.0000", "200.0000", "200.0000", "200.0000", "200.0000", "250.0000", "250.0000", "250.0000", "200.0000", "200.0000", "200.0000", "200.0000", "250.0000", "250.0000", "250.0000", "200.0000", "200.0000", "200.0000", "200.0000", "250.0000", "250.0000", "250.0000", "200.0000"];
              roomInfo1.set({dailyPrices: prices1});
              this.add(roomInfo1);

              ////第3个房间
              //var roomInfo2 = new RoomInfo();
              //roomInfo2.set({roomId: 867900, roomNumber: '106', roomTypeId: 340518, roomType: '芈月古典房'});

              //var orderInfo3 = new OrderInfo();
              //orderInfo3.set({
              //    orderId:11514423,
              //    stayIn: "2016-09-15 00:00:00",
              //    stayOut:"2016-09-17 00:00:00",
              //    fromRNightDate: "2016-09-15 00:00:00",
              //    toRNightDate: "2016-09-16 00:00:00",
              //    roomId: 867900,
              //    roomStatus: RoomStatus.Booked,
              //    checkoutAmount: 300.00,
              //    paymentType: PaymentType.Cash,
              //    arrivalDate: "2016-09-15",
              //    arrivalTime: "18:00",
              //    departureDate: "2016-09-17",
              //    roomNights: 2,
              //    customerName: "刘凯威",
              //    customerMobile: "13817474370",
              //    mobileArea: "上海",
              //    amount: 300.00,
              //    channel: ChannelType.Guest,
              //    orderDetailId: 28395545,
              //    prePayAmount: 0
              //});

              //var orderInfo4 = new OrderInfo();
              //orderInfo4.set({
              //    orderId:11946406,
              //    stayIn: "2016-09-25 00:00:00",
              //    stayOut:"2016-09-26 00:00:00",
              //    fromRNightDate: "2016-09-25 00:00:00",
              //    toRNightDate: "2016-09-25 00:00:00",
              //    roomId: 867900,
              //    roomStatus: RoomStatus.Away,
              //    checkoutAmount: 300.00,
              //    paymentType: PaymentType.Cash,
              //    arrivalDate: "2016-09-13",
              //    arrivalTime: "18:00",
              //    departureDate: "2016-09-14",
              //    roomNights: 1,
              //    customerName: "姜子牙",
              //    customerMobile: "13696967255",
              //    mobileArea: "厦门",
              //    amount: 300.00,
              //    channel: ChannelType.Guest,
              //    orderDetailId: 98778833,
              //    prePayAmount: 0
              //});


              //var orderInfos2 = new OrderInfos();
              //orderInfos2.add(orderInfo3);
              //orderInfos2.add(orderInfo4);
              //roomInfo2.set({orders: orderInfos2});

              //var prices2 = ["450.0000", "350.0000", "350.0000", "350.0000", "350.0000", "450.0000", "450.0000", "450.0000", "350.0000", "350.0000", "350.0000", "350.0000", "450.0000", "450.0000", "450.0000", "350.0000", "350.0000", "350.0000", "350.0000", "450.0000", "450.0000", "450.0000", "350.0000", "350.0000", "350.0000", "350.0000", "450.0000", "450.0000", "450.0000", "350.0000"];
              //roomInfo2.set({dailyPrices: prices2});
              //this.add(roomInfo2);
          }
      });

      return roomInfos;
  });
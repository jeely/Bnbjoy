/**
 * Created by lijizhuang on 16/9/6.
 */
//可预定的日期和房间数
define(['jquery', 'underscore', 'backbone', 'common', 'extensions/dateExts'], function ($, _, Backbone, Common, DateExts) {
    var availableRoom = Backbone.Model.extend({
        defaults: {
            date : '',
            MMdd : '',
            totalRooms:'',
            availableRooms : '',
            dayOfWeek:'',
            isHoliday:false
        },

        initialize: function () {
            _this = this;
            this.on('change:date', function (model, value) {
                var currentDate = new Date(value);
                _this.set('MMdd', currentDate.MMdd());
                var today = new Date();
                if(currentDate.getFullYear() === today.getFullYear() && currentDate.getMonth() === today.getMonth()
                  && currentDate.getDate() === today.getDate()){
                    //_this.set('isToday', true);
                    _this.set('dayOfWeek', '今天');
                }
                else {
                    //设置周几或节日
                    var justDayOfWeek = Common.getDayOfWeek(value);
                    _this.set('dayOfWeek', justDayOfWeek);
                    var justFestival = Common.festivalCheck(currentDate.getMonth() + 1, currentDate.getDate(), _this.get('dayOfWeek'));
                    if (justDayOfWeek !== justFestival) {
                        _this.set('isHoliday', true);
                    }
                    _this.set('dayOfWeek', justFestival);
                }
            });
        },

    });

    return availableRoom;
});
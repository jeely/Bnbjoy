/**
 * Created by lijizhuang on 16/9/8.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var roomInfo = Backbone.Model.extend({
        defaults: {
            roomId: '',      //房间id
            roomNumber: '',  //房号/房间名
            roomTypeId:'',   //房型id
            roomType:'',     //房型
            roomTypeRank:'', //房型排名
            roomRank:'',    //房间排名
            orders: null,   //该房间关联的订单号(数组)
            dailyPrices: null,    //该房间在这段时间每天的价格(数组)
        },
        
        initialize: function () {

        }
    });

    return roomInfo;
});
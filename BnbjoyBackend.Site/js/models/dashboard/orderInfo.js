/**
 * Created by lijizhuang on 16/9/8.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var orderInfo = Backbone.Model.extend({
        defaults: {
            orderId: '', //订单号
            stayIn:'',  //入住时间
            stayOut:'', //离店时间
            fromRNightDate: '', //入住(间夜)
            toRNightDate: '',  //离开(间夜)
            roomId:'',          //房间id
            roomStatus:'',      //房间状态
            checkoutAmount: '', //离店结算
            paymentType:'',   //支付类型
            arrivalDate:'',  //到达时间(yyyy-MM-dd)
            arrivalTime:'', //到达时间(hh:mm)
            departureDate:'', //离开时间(yyyy-MM-dd)
            roomNights:'', //间夜数
            costomerName:'', //预定人姓名
            costomerMobile:'', //预定人手机
            mobileArea:'', //手机号归属地
            amount:'', //金额
            channel:'', //入住渠道
            orderDetailId:'', //详细订单号(关联)
            prePayAmount:'' //预付费用
        },
        
        initialize: function () {
            
        }
    });

    return orderInfo;
});
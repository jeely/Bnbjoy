/**
 * Created by lijizhuang on 16/11/1.
 */
define(['jquery','underscore', 'backbone', 'models/settings/account/bnbPermissionItem'], function ($, _, Backbone, BnbPermissionItem) {
    var BnbPermissionCollection = Backbone.Collection.extend({
        model: BnbPermissionItem,

        initialize: function () {

        }
    });

    return BnbPermissionCollection;
});
/**
 * Created by lijizhuang on 16/9/14.
 */
define(['jquery','underscore','backbone'], function ($, _, Backbone) {
    var OwnerInfo = Backbone.Model.extend({
        defaults: {
            userName: '',
            realName: '',
            password:'',
            email:'',
            mobile:'',
            bindId:false,  //是否绑定身份证
            bindCert:false  //是否绑定证书
        },

        initialize: function () {

        }
    });

    return OwnerInfo;
});
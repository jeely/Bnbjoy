﻿/**
 * Created by lijizhuang on 16/9/13.
 */
require(['require.config'], function (Config) {
    require(['jquery', 'underscore', 'backbone', 'common', 'url', 'models/dashboard/bookableRoom', 'models/dashboard/bookableRooms',
            'modules/dashboard/mainTable/theader', 'models/dashboard/roomInfos', 'modules/dashboard/mainTable/dashTable', 'plugins/jedate'],
        function ($, _, Backbone, Common, Url, BookableRoom, BookableRooms, TheaderView, RoomInfos, DashTableView, jeDate) {

            //var param = new Date().yyyyMMdd();
            //var bookableRooms = new BookableRooms();
            //bookableRooms.requestAsync(param);
            //var theaderView = new TheaderView({ collection: bookableRooms });
            //$('.availableInfo').append(theaderView.render().el);

            //var roomInfos = new RoomInfos();
            //roomInfos.requestAsync(param);
            //var dashTableView = new DashTableView({ collection: roomInfos, dateStr: param });
            //$('.dashboard').append(dashTableView.render().el);

            main(Common, Url, BookableRoom, BookableRooms, TheaderView, RoomInfos, DashTableView);

            $('.logininfo a:last').click(function (e) {
                e.preventDefault();
                $(this).text("注销中");
                //​$(this).contents().unwrap();​​
                Common.sendAjaxRequest(Url.logout, "", function (data) {
                    window.location.href = "Account/Login";
                }, function (data) {
                    if (data.Content && data.Content.length > 0) {
                        $(this).text("退出");
                    }
                }, false);
            });

            jeDate({
                dateCell: '#datePick',
                festival: true,
                isinitVal: true,
                isClear: false,
                isToday: false,
                format: 'YYYY-MM-DD',
                choosefun: function (elem, val) {

                    //时间选择后重新获取数据
                    var bookableRooms = new BookableRooms();
                    bookableRooms.requestAsync(val);
                    var theaderView = new TheaderView({ collection: bookableRooms });
                    $('.timeAxis').replaceWith(theaderView.render().el);
                }
            });
        });
});

function main(Common, Url, BookableRoom, BookableRooms, TheaderView, RoomInfos, DashTableView){
    Common.sendAjaxRequest(Url.currentBnb, "", function (data) {
        if (data && data.Content && data.Content.length > 0) {
            //获取bnb id
            var bnb = JSON.parse(data.Content);
            var bnbId = bnb.BnbId;
            
            var dateStr = new Date().yyyyMMdd();
            //获取可预订房间的信息
            syncAvailableRooms(Common, Url, BookableRoom, BookableRooms, TheaderView, bnbId, dateStr);

            var roomInfos = new RoomInfos();
            roomInfos.requestAsync(dateStr);
            var dashTableView = new DashTableView({ collection: roomInfos, dateStr: dateStr, bnbId: bnbId });
            $('.dashboard').append(dashTableView.render().el);
        }
    }, function (data) {
            
    }, false);
}

function syncAvailableRooms(Common, Url, BookableRoom, BookableRooms, TheaderView, bnbId, dateStr) {
    var bookableRooms = new BookableRooms();
    //bookableRooms.reset();
    var paramModel = {};
    paramModel.bnbId = bnbId;
    paramModel.fromDate = dateStr;
    Common.sendAjaxRequest(Url.availableRooms, paramModel, function (data) {
        if (data && data.Content && data.Content.length > 0) {
            var infos = JSON.parse(data.Content);
            var total = infos.total;
            for(var p in infos.pairs){
                var bookableRoom = new BookableRoom();
                bookableRoom.set({ date: infos.pairs[p].date, totalRooms: total, availableRooms: infos.pairs[p].roomCount });
                bookableRooms.add(bookableRoom);
            }
            var theaderView = new TheaderView({ collection: bookableRooms });
            $('.availableInfo').append(theaderView.render().el);
        }
    }, function (data) {

    }, false);

    //ajax请求获取特定时间段的可预定房间
    //mock接口
    //var fromDate = new Date(fromDateStr);
    //for (var i = 0; i < 30; i++) {
    //    var currentDate = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate() + i);

    //    var bookableRoom = new BookableRoom();
    //    bookableRoom.set({ date: currentDate, totalRooms: 4, availableRooms: 3 });
    //    this.add(bookableRoom);
    //}
}
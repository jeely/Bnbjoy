﻿require(['require.config'], function (Config) {
    require(['jquery', 'underscore', 'backbone', 'common', 'url', 'modules/settings/account/ownerInfo', 'modules/settings/account/employeesInfo',
        'models/settings/account/ownerInfo', 'models/settings/account/employee', 'models/settings/account/employees'],
    function ($, _, Backbone, Common, Url, OwnerInfoView, EmployeesInfoView, OwnerInfo, Employee, Employees) {

        $('.logininfo a:last').click(function (e) {
            e.preventDefault();
            $(this).text("注销中");
            //​$(this).contents().unwrap();​​
            Common.sendAjaxRequest(Url.logout, "", function (data) {
                window.location.href = "Account/Login";
            }, function (data) {
                if (data.Content && data.Content.length > 0) {
                    $(this).text("退出");
                }
            }, false);
        });

        var ownerAndEmployeesInfo;

        Common.sendAjaxRequest(Url.ownerandemployees, "", function (data) {
            if (data != undefined && data.Content != undefined) {
                ownerAndEmployeesInfo = JSON.parse(data.Content);

                var ownerInfo = new OwnerInfo();
                ownerInfo.set({
                    ownerId: ownerAndEmployeesInfo.ownerId,
                    userName: ownerAndEmployeesInfo.ownerName,
                    realName: ownerAndEmployeesInfo.ownerRealName,
                    email: ownerAndEmployeesInfo.ownerEmail,
                    mobile: ownerAndEmployeesInfo.ownerMobile,
                    bindId: ownerAndEmployeesInfo.ownerIdCard,
                    bindCert: ownerAndEmployeesInfo.ownerCert
                });
                var ownerInfoView = new OwnerInfoView({ model: ownerInfo });
                $('.accountSetting').append(ownerInfoView.render().el);
                $('.accountSetting').append('<div class="sp"></div>');

                var employees = new Employees();
                if (ownerAndEmployeesInfo.employees && ownerAndEmployeesInfo.employees.length > 0) {
                    for (var e in ownerAndEmployeesInfo.employees) {
                        var eObj = ownerAndEmployeesInfo.employees[e];
                        var permissionList = [];

                        if (eObj.employeePermission && eObj.employeePermission.length > 0)
                        {
                            for (var p in eObj.employeePermission) {
                                var item = {};
                                var pObj = eObj.employeePermission[p];
                                //var rightsArr = pObj.employeeRights.split(',');
                                var rightsList = [];

                                item.bnbId = pObj.bnbId;
                                item.bnbName = pObj.bnbName;
                                //将权限由数字转成特定键值对列表
                                rightsList = Common.convertPermissionFromStr(pObj.employeeRights);
                                item.rights = rightsList;
                                permissionList.push(item);
                            }
                        }

                        var employee = new Employee();
                        employee.set({
                            employeeid: eObj.employeeId,
                            userName: eObj.employeeName,
                            realName: eObj.employeeRealName,
                            email: eObj.employeeEmail,
                            mobile: eObj.employeeMobile,
                            enabled: eObj.enabled,
                            permission: permissionList
                        });

                        employees.add(employee);
                    }

                    var employeesInfoView = new EmployeesInfoView({ collection: employees, ownerId: ownerInfo.get('ownerId') });
                    $('.accountSetting').append(employeesInfoView.render().el);
                }
            }
        }, function (data) {
            if (data.Content && data.Content.length > 0) {
                //请求加载数据失败
            }
        }, false);
    })
});
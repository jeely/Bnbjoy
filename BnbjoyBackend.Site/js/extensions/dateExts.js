/**
 * Created by lijizhuang on 16/9/8.
 */
define(function () {
    //年月日
    Date.prototype.yyyyMMdd = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString();
        var dd = this.getDate().toString();

        var mmChars = mm.split('');
        var ddChars = dd.split('');

        var datestring = yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]) + '-' + (ddChars[1] ? dd : "0" + ddChars[0]);
        return datestring;
    };

    //年月
    Date.prototype.yyyyMM = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString();

        var mmChars = mm.split('');
        var datestring = yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]);
        return datestring;
    },

    //月日
    Date.prototype.MMdd = function () {
        var mm = (this.getMonth() + 1).toString();
        var dd = this.getDate().toString();

        var mmChars = mm.split('');
        var ddChars = dd.split('');

        var datestring = (mmChars[1] ? mm : "0" + mmChars[0]) + '-' + (ddChars[1] ? dd : "0" + ddChars[0]);
        return datestring;
    };

    Date.prototype.tomorrow = function () {
        this.setDate(this.getDate() + 1);
        return this;
    },

    Date.prototype.yesterday = function () {
        this.setDate(this.getDate() - 1);
        return this;
    },

    Date.prototype.twoMonthLater = function () {
        this.setMonth(this.getMonth() + 2);
        return this;
    };

    //得到两个时间相差的天数
    Date.prototype.diffInDays = function (date2) {
        var _MS_PER_DAY = 1000 * 60 * 60 * 24;
        var utc1 = Date.UTC(this.getFullYear(), this.getMonth(), this.getDate());
        var utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
        return Math.floor(Math.abs(utc2 - utc1) / _MS_PER_DAY);
    };

    return Date;
});


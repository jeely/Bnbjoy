﻿/**
 * Created by lijizhuang on 16/9/13.
 */
require(['require.config'], function (Config) {
    require(['models/registerAndLogin/loginStatus', 'modules/registerAndLogin/login'], function (LoginStatus, LoginView) {
        var loginStatus = new LoginStatus();
        var loginView = new LoginView({ model: loginStatus });
        $('body').append(loginView.render().el);
    });
});
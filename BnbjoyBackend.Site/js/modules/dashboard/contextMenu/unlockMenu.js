/**
 * Created by lijizhuang on 16/9/4.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/contextMenu/unlockMenuView.html'], function ($, _, Backbone, UnlockMenuViewTemplate) {
    var unlockMenuView = Backbone.View.extend({
        tagName:'ul',
        id: 'unlockMenu',
        className: 'contextMenu',

        template: _.template(UnlockMenuViewTemplate),

        initialize: function () {
            _this = this;
        },

        events: {
            'click #unlockRoom':'onUnlockRoom'
        },

        onUnlockRoom: function () {
            console.log('取消关房');
            _this.removeView();
        },

        render: function () {
            var model = this.model;
            $(this.el).html(this.template(model));
            return this;
        },

        showView: function () {
            $(_this.el).show().css({
                'top': _this.model.top + 'px',
                'left': _this.model.left + 'px'
            });
        },

        removeView:function(){
            $(_this.el).remove();
        }
    });

    return unlockMenuView;
});
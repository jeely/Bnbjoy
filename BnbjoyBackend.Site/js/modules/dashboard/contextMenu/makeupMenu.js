/**
 * Created by lijizhuang on 16/9/3.
 */
//可以弥补登记
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/contextMenu/makeupMenuView.html'], function ($, _, Backbone, MakeupMenuViewTemplate) {
    var makeupMenuView = Backbone.View.extend({
        tagName:'ul',
        id: 'makeupMenu',
        className: 'contextMenu',

        template: _.template(MakeupMenuViewTemplate),

        initialize: function () {
            _this = this;
        },

        events: {
            'click #makeupOrder':'onMakeupOrder'
        },

        onMakeupOrder: function () {
            console.log('补录订单');
            this.removeView();
        },

        render: function () {
            var model = this.model;
            $(this.el).html(this.template(model));
            return this;
        },

        showView: function () {
            $(_this.el).show().css({
                'top': _this.model.top + 'px',
                'left': _this.model.left + 'px'
            });
        },

        removeView:function(){
            $(_this.el).remove();
        }
    });

    return makeupMenuView;
});
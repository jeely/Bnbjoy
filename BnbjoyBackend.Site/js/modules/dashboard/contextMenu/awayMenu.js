/**
 * Created by lijizhuang on 16/9/3.
 */
//已离开
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/contextMenu/awayMenuView.html'], function ($, _, Backbone, AwayMenuViewTemplate) {
    var awayMenuView = Backbone.View.extend({
        tagName:'ul',
        id: 'awayMenu',
        className: 'contextMenu',

        template: _.template(AwayMenuViewTemplate),

        initialize: function () {
            _this = this;
        },

        events: {
            'click #viewOrder':'onViewOrder',
        },

        onViewOrder: function () {
            console.log('查看订单');
            this.removeView();

        },

        render: function () {
            var model = this.model;
            $(this.el).html(this.template(model));
            return this;
        },

        showView: function () {
            $(_this.el).show().css({
                'top': _this.model.top + 'px',
                'left': _this.model.left + 'px'
            });
        },

        removeView:function(){
            $(_this.el).remove();
        }
    });

    return awayMenuView;
});
/**
 * Created by lijizhuang on 16/9/3.
 */
//已预定,当日可办入住
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/contextMenu/canCheckinMenuView.html'], function ($, _, Backbone, CanCheckinMenuViewTemplate) {
    var canCheckinMenuView = Backbone.View.extend({
        tagName:'ul',
        id: 'canCheckinMenu',
        className: 'contextMenu',

        template: _.template(CanCheckinMenuViewTemplate),

        initialize: function () {
            _this = this;
        },

        events: {
            'click #viewOrder':'onViewOrder',
            'click #handleCheckin' : 'onHandleCheckin',
            'click #cancelOrder': 'onCancelOrder'
        },

        onViewOrder: function () {
            console.log('查看订单');
            this.removeView();

        },

        onHandleCheckin: function () {
            console.log('办理入住');
            this.removeView();
        },

        onCancelOrder: function () {
            console.log('取消订单');
            this.removeView();
        },

        render: function () {
            var model = this.model;
            $(this.el).html(this.template(model));
            return this;
        },

        showView: function () {
            $(_this.el).show().css({
                'top': _this.model.top + 'px',
                'left': _this.model.left + 'px'
            });
        },

        removeView:function(){
            $(_this.el).remove();
        }
    });

    return canCheckinMenuView;
});
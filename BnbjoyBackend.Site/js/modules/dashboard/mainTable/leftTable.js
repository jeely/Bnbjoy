/**
 * Created by lijizhuang on 16/9/9.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/mainTable/leftTableView.html'], function ($, _, Backbone, LeftTableViewTemplate) {
    var leftTableView = Backbone.View.extend({
        tagName: 'table',
        className: 'leftTable',
        template: _.template(LeftTableViewTemplate),
        
        events:{
            
        },
        
        render: function () {
            var collection = this.collection;
            var collectGroup = this.collection.groupBy(function (o) {
                return o.attributes.roomType; //按照房型名称分组
            });

            var collectGroup = _.map(collectGroup, function (value, key) {
                return {key:key, value:value};
            });

            var content = this.template({value: collectGroup});
            $(this.el).html(content);
            return this;
        }
    });

    return leftTableView;
});
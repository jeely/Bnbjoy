/**
 * Created by lijizhuang on 16/9/5.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/headerAndNavigator/headerView.html'],
    function ($, _, Backbone, HeaderViewTemplate) {
        var headerView = Backbone.View.extend({
            tagName: 'div',
            className: 'header',

            template: _.template(HeaderViewTemplate),

            initialize: function () {
                _this = this;
            },

            events: {
                'click #enableOnsale': 'onEnableOnsale',
                'click #disableOnsale': 'onDisableOnsale',
            },

            onEnableOnsale: function () {
                console.log('开启线上销售');
                this.model.set('onsale', true);
            },

            onDisableOnsale: function () {
                console.log('关闭线上销售');
                this.model.set('onsale', false);
            },

            render: function () {
                $(this.el).html(this.template(this.model.attributes));
                return this;
            }
        });

        return headerView;
    });
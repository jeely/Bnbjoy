/**
 * Created by lijizhuang on 16/9/5.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/headerAndNavigator/subnavView.html'],
    function ($, _, Backbone, SubNavViewTemplate) {
        var subNavView = Backbone.View.extend({
            tagName: 'div',
            className: 'second-menu',

            template: _.template(SubNavViewTemplate),

            initialize: function () {
                _this = this;
                
            },

            events:{
                'click #accountmgmt': 'onAccountMgmt',
                'click #smsmgmt' : 'onSmsMgmt',
                'click #paymentmgmt' : 'onPaymentMgmt',
                'click #personalmgmt' : 'onPersonalMgmt'
            },

            onAccountMgmt: function () {
                console.log('帐号管理');

                $('.seconditem-selected').removeClass('seconditem-selected');
                $('#accountmgmt').parent().addClass('seconditem-selected');
            },

            onSmsMgmt: function () {
                console.log('短信管理');

                $('.seconditem-selected').removeClass('seconditem-selected');
                $('#smsmgmt').parent().addClass('seconditem-selected');
            },

            onPaymentMgmt: function () {
                console.log('支付方式管理');

                $('.seconditem-selected').removeClass('seconditem-selected');
                $('#paymentmgmt').parent().addClass('seconditem-selected');
            },

            onPersonalMgmt: function () {
                console.log('个性化设置');

                $('.seconditem-selected').removeClass('seconditem-selected');
                $('#personalmgmt').parent().addClass('seconditem-selected');
            },

            render: function () {
                var temp = this.template(this.model);
                this.$el.html(temp);

                return this;
            }
        });

        return subNavView;
    }
);

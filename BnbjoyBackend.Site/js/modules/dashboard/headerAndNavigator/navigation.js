/**
 * Created by lijizhuang on 16/9/5.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/headerAndNavigator/navigationView.html', 'modules/dashboard/headerAndNavigator/subnav'],
    function ($, _, Backbone, NavigationViewTemplate, SubNavView) {
        var navigationView = Backbone.View.extend({
            tagName: 'div',
            className: 'nav',

            template: _.template(NavigationViewTemplate),

            initialize: function () {
                _this = this;
                //var items = this.model.get('items');
                //if(items !== undefined&& items !== null && items.length>0)
                //    firstItem = this.model.get('items')[0];
            },

            events:{
                'click #dashmgmt': 'onDashMgmt',
                'click #ordermgmt':'onOrderMgmt',
                'click #infomgmt':'onInfoMgmt',
                'click #roommgmt':'onRoomMgmt',
                'click #socialmgmt':'onSocialMgmt',
                'click #visitormgmt':'onVisitorMgmt',
                'click #statsmgmt':'onStatsMgmt',
                'click #settingsmgmt':'onSettingsMgmt'
            },

            clearItemSelection: function () {
                //$('.first-menu').children('li').each(function () {
                //    $(this).removeClass('mainitem-selected');
                //});
                $('.mainitem-selected').removeClass('mainitem-selected');

                $('.second-menu').html('');
            },

            onDashMgmt: function (e) {
                console.log('前台管理');

                this.clearItemSelection();
                $('#dashmgmt').parent().addClass('mainitem-selected');
            },

            onOrderMgmt: function () {
                console.log('订单管理');

                this.clearItemSelection();
                $('#ordermgmt').parent().addClass('mainitem-selected');
            },

            onInfoMgmt: function () {
                console.log('民宿信息');

                this.clearItemSelection();
                $('#infomgmt').parent().addClass('mainitem-selected');
            },

            onRoomMgmt: function () {
                console.log('房价房态');

                this.clearItemSelection();
                $('#roommgmt').parent().addClass('mainitem-selected');
            },

            onSocialMgmt: function () {
                console.log('社交管理');

                this.clearItemSelection();
                $('#socialmgmt').parent().addClass('mainitem-selected');
            },

            onVisitorMgmt: function () {
                console.log('客人信息');

                this.clearItemSelection();
                $('#visitormgmt').parent().addClass('mainitem-selected');
            },

            onStatsMgmt: function () {
                console.log('信息统计');

                this.clearItemSelection();
                $('#statsmgmt').parent().addClass('mainitem-selected');
            },

            onSettingsMgmt: function () {
                console.log('设置');

                this.clearItemSelection();
                $('#settingsmgmt').parent().addClass('mainitem-selected');

                var subMenuView = new SubNavView({model: {value: 'settingsmgmt'}});
                var content = subMenuView.render().el;
                $('.second-nav').append(content);

            },

            render: function () {
                $(this.el).html(this.template(this.model));

                if(this.model.nav == 'settingsmgmt'){
                    var subMenuView = new SubNavView({model: {value: 'settingsmgmt'}});
                    var content = subMenuView.render().el;
                    this.$('.second-nav').append(content);
                }

                return this;
            }
        });

        return navigationView;
    });
/**
 * Created by lijizhuang on 16/9/14.
 */
define(['jquery', 'underscore', 'backbone', 'bootstrap', 'text!modules/settings/account/ownerInfoView.html', 'modules/settings/account/modal/changePassword',
        'modules/settings/account/modal/changeEmail', 'modules/settings/account/modal/changeMobile'],
    function ($, _, Backbone, Bootstrap, OwnerInfoViewTemplate, ChangePasswordView, ChangeEmailView, ChangeMobileView) {
        var ownerInfoView = Backbone.View.extend({
            tagName: 'div',
            className: 'ownerInfo',
            template: _.template(OwnerInfoViewTemplate),

            initialize: function () {
                _ownerInfoView = this;
                this.model.on('change', function (model, value){
                    _ownerInfoView.render();
                });
            },

            events: {
                'click .password a':'onChangePassword',
                'click .email a':'onChangeEmail',
                'click .mobile a': 'onChangeMobile'
            },

            onChangePassword: function (e) {
                //修改密码弹层
                var changePasswordView = new ChangePasswordView({model: this.model});
                if($('body').find('#changePassword').length === 0) {
                    $('body').append(changePasswordView.render().el);
                }
                else{
                    $('#changePassword').replaceWith(changePasswordView.render().el);
                }

                $("#changePassword").modal({backdrop: 'static', keyboard: false});
            },

            onChangeEmail: function (e) {
                //修改邮箱弹层
                var changeEmailView = new ChangeEmailView({model: this.model});
                if($('body').find('#changeEmail').length === 0) {
                    $('body').append(changeEmailView.render().el);
                }
                else{
                    $('#changeEmail').replaceWith(changeEmailView.render().el);
                }

                $("#changeEmail").modal({backdrop: 'static', keyboard: false});
            },

            onChangeMobile: function () {
                var changeMobileView = new ChangeMobileView({model: this.model});
                if($('body').find('#changeMobile').length === 0) {
                    $('body').append(changeMobileView.render().el);
                }
                else{
                    $('#changeMobile').replaceWith(changeMobileView.render().el);
                }

                $("#changeMobile").modal({backdrop: 'static', keyboard: false});
            },

            render: function () {
                this.$el.html(this.template(this.model.attributes));
                
                return this;
            }

        });

        return ownerInfoView;
    });
/**
 * Created by lijizhuang on 16/9/17.
 */
define(['jquery', 'underscore', 'backbone', 'bootstrap', 'text!modules/settings/account/modal/changeNameView.html', 'utils/regutil'],
    function ($, _, Backbone, Bootstrap, ChangeEmailViewTemplate, RegUtil) {
        var changeNameView = Backbone.View.extend({
            tagName: 'div',
            id: 'changeName',
            className: 'modal fade',
            template: _.template(ChangeEmailViewTemplate),

            initialize: function () {

            },

            events:{
                'click #saveBtn':'onSaveButtonClick'
            },

            onSaveButtonClick:function(e){
                var newName = $.trim($('#newName').val());

                var errMsg = "";
                this.$('.change-name-body .errorMsg').html('');

                if(newName.length === 0){
                    //如果姓名为空
                    errMsg = "*姓名不能为空";
                }
                else if(!RegUtil.chineseNameReg.test(newName)){
                    //姓名格式错误
                    errMsg = "*姓名格式错误";
                }

                if(errMsg.length > 0){
                    this.$('.change-name-body .errorMsg').css('color','red');
                    this.$('.change-name-body .errorMsg').html(errMsg);
                }
                else{
                    //发送请求修改手机

                    //成功后
                    this.$el.modal('hide');
                    this.model.set('realName', $('#newName').val());

                    //清空表单
                    $('#newName').val('');
                }
            },

            render:function(){
                this.$el.html(this.template(this.model.attributes));

                return this;
            }

        });

        return changeNameView;
});

/**
 * Created by lijizhuang on 16/9/17.
 */
define(['jquery', 'underscore', 'backbone', 'bootstrap', 'text!modules/settings/account/modal/changeMobileView.html', 'utils/regutil'],
    function ($, _, Backbone, Bootstrap, ChangeMobileViewTemplate, RegUtil) {
        var changeMobileView = Backbone.View.extend({
            tagName: 'div',
            id: 'changeMobile',
            className: 'modal fade',
            template: _.template(ChangeMobileViewTemplate),

            initialize: function () {
                _changeMobileView = this;
                _changeMobileView.killTimer();
                this.model.on('change:mobile', function (model, value){
                    _changeMobileView.render();
                });
            },

            events:{
                'click #saveBtn':'onSaveButtonClick',
                'click #getMobileCaptcha':'onGetCaptchaClick'
            },

            checkNewMobile: function () {
                var newMobile = $.trim($('#newMobile').val());
                var captcha = $.trim($('#mobileCaptcha').val());

                var errMsg = "";
                this.$('.change-mobile-body .errorMsg').html('');

                if(newMobile.length === 0){
                    //如果新手机号为空
                    errMsg = "*新手机号不能为空";
                }
                else if(!RegUtil.mobileReg.test(newMobile)){
                    //新手机号格式错误
                    errMsg = "*手机号格式填写错误";
                }
                else if(captcha.length === 0){
                    //验证码为空
                    errMsg = "*验证码不能为空";
                }

                if(errMsg.length > 0){
                    this.$('.change-mobile-body .errorMsg').css('color','red');
                    this.$('.change-mobile-body .errorMsg').html(errMsg);
                    return false;
                }

                return true;
            },

            onSaveButtonClick: function () {
                if(this.checkNewMobile()){
                    //发送请求修改手机

                    //成功后
                    this.$el.modal('hide');
                    this.model.set('mobile', $('#newMobile').val());

                    //清空表单
                    $('#newMobile').val('');
                    $('#mobileCaptcha').val('');
                }
            },

            onGetCaptchaClick: function () {
                var newMobile = $.trim($('#newMobile').val());

                var errMsg = "";
                this.$('.change-mobile-body .errorMsg').html('');
                this.$('.change-mobile-body .errorMsg').css('color','red');
                if(newMobile.length === 0){
                    //如果新手机号为空
                    errMsg = "*新手机号不能为空";
                }
                else if(!RegUtil.mobileReg.test(newMobile)){
                    //新手机号格式错误
                    errMsg = "*手机号格式填写错误";
                }

                if(errMsg.length > 0){
                    this.$('.change-mobile-body .errorMsg').html(errMsg);
                    return false;
                }else{
                    this.$('.change-mobile-body .errorMsg').css('color','#4d4d4d');
                    this.$('.change-mobile-body .errorMsg').html('验证码已发送至手机:'+ newMobile);

                    //发送验证码请求,设置获取倒计时
                    var second = 60;

                    $('#getMobileCaptcha').hide();
                    $('#resetMobileCaptcha').val(second + '秒后重获取');
                    $('#resetMobileCaptcha').show();

                    window.getMobileCaptchaTimer = setInterval(function () {
                        second -= 1;
                        if (second > 0) {
                            $('#resetMobileCaptcha').val(second + '秒后重获取');
                        } else {
                            clearInterval(getMobileCaptchaTimer);
                            $('#getMobileCaptcha').show();
                            $('#resetMobileCaptcha').hide();
                        }
                    }, 1000);
                }
            },

            killTimer: function () {
                if(window.getMobileCaptchaTimer != undefined || window.getMobileCaptchaTimer != null){
                    clearInterval(getMobileCaptchaTimer);
                }
            },

            render: function () {
                this.$el.html(this.template(this.model.attributes));

                return this;
            }

        });

        return changeMobileView;
    }
);

/**
 * Created by lijizhuang on 16/9/16.
 */
define(['jquery', 'underscore', 'backbone', 'bootstrap', 'text!modules/settings/account/modal/changePasswordView.html', 'utils/regutil'],
    function ($, _, Backbone, Bootstrap, ChangePasswordViewTemplate, RegUtil) {
    var changeEmailView = Backbone.View.extend({
        tagName: 'div',
        id: 'changePassword',
        className: 'modal fade',
        template: _.template(ChangePasswordViewTemplate),

        initialize: function () {

        },

        events:{
            'click #saveBtn':'onSaveButtonClick'
        },

        onSaveButtonClick: function () {
            var oriPassword = $.trim($('#oriPassword').val());
            var newPassword = $.trim($('#newPassword').val());
            var confirmPassword = $.trim($('#confirmPassword').val());

            var errMsg = "";
            this.$('.change-password-body .errorMsg').html('');

            if(oriPassword.length === 0) {
                //如果原密码为空
                errMsg = "*原密码不能为空";

            }
            else if(!RegUtil.passwordReg.test(oriPassword)){
                //原密码格式错误
                errMsg = "*原密码格式错误";
            }
            else if(newPassword.length === 0){
                //如果新密码为空
                errMsg = "*新密码不能为空";
            }
            else if(!RegUtil.passwordReg.test(newPassword)){
                //新密码格式错误
                errMsg = "*新密码格式错误";
            }
            else if(confirmPassword.length === 0){
                errMsg = "*请再次输入新密码";
            }
            else if(confirmPassword !== newPassword){
                errMsg = "*新密码确认不一致";
            }

            if(errMsg.length > 0){
                this.$('.change-password-body .errorMsg').html(errMsg);
            }
            else {
                //提交服务端进行密码修改

                //密码修改成功后要取消弹层,并且清空弹层里的表单
                this.$el.modal('hide');

                //清空表单
                $('#oriPassword').val('');
                $('#newPassword').val('');
                $('#confirmPassword').val('');
            }
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        }
    });

    return changeEmailView;
});
﻿/**
 * Created by lijizhuang on 16/10/31.
 */
define(['jquery', 'underscore', 'backbone', 'bootstrap', 'common', 'url', 'models/registerAndLogin/newEmployeeInfo', 'text!modules/settings/account/modal/newEmployeeAccountView.html', 'utils/regutil'],
    function ($, _, Backbone, Bootstrap, Common, Url, NewEmployeeInfo, NewEmployeeAccountTemplate, RegUtil) {
    var newEmployeeAccountView = Backbone.View.extend({
        tagName: 'div',
        id: 'newEmployeeAccount',
        className: 'modal fade',
        template: _.template(NewEmployeeAccountTemplate),
        validateSuccess: true,

        initialize: function (attrs) {
            employeeInfo = new NewEmployeeInfo();

            this.options = attrs;
        },

        events:{
            'click #saveBtn':'onSaveButtonClick'
        },

        onSaveButtonClick: function (e) {
            this.validateSuccess = true;
            var errMsg = "";

            employeeInfo.on('invalid', _.bind(function (model, error) {
                //注册表单验证结果处理
                var errorArr = JSON.parse(error);
                if(errorArr.length > 0){
                    errMsg = "*" + errorArr[0].error;
                }

                this.validateSuccess = false;
            }, this));

            employeeInfo.set({
                //employeeid: '123456',
                userName: $('#employeeAccount').val(),
                password: $('#employeePassword').val(),
                email: $('#employeeEmail').val(),
                mobile: $('#employeeMobile').val(),
                realName: $('#employeeRealName').val(),
                ownerId:this.model,
                enabled: true,
                roleName: "BE",
                permission:[],
            }, {'validate': true});

            if(!this.validateSuccess){
                this.$('.new-employee-body .errorMsg').html(errMsg);
            }
            else {
                //提交服务端进行密码修改
                Common.sendAjaxRequest(Url.createNewEmployee, employeeInfo.attributes, _.bind(function (data) {
                    //如果新建employee成功，服务端返回employeeid
                    if (data.Content && data.Content.length > 0) {
                        var resObj = JSON.parse(data.Content);
                        employeeInfo.set({
                            employeeid: resObj.employeeid
                        });

                        this.options.collection.add(employeeInfo);
                    }
                }, this), _.bind(function (data) {
                    if (data.Content && data.Content.length > 0) {
                        this.$('.new-employee-body .errorMsg').html(data.Content);
                    }
                }, this), false);

                //密码修改成功后要取消弹层,并且清空弹层里的表单
                this.$el.modal('hide');

                //清空表单
                $('#employeeAccount').val('');
                $('#employeePassword').val('');
                $('#employeeRealName').val('');
                $('#employeeMobile').val('');
                $('#employeeEmail').val('');
            }
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        }
    });

        return newEmployeeAccountView;
});
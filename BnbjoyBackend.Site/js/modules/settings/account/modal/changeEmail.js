/**
 * Created by lijizhuang on 16/9/16.
 */
define(['jquery', 'underscore', 'backbone', 'bootstrap', 'text!modules/settings/account/modal/changeEmailView.html', 'utils/regutil'],
    function ($, _, Backbone, Bootstrap, ChangeEmailViewTemplate, RegUtil) {
        var changeEmailView = Backbone.View.extend({
            tagName: 'div',
            id: 'changeEmail',
            className: 'modal fade',
            template: _.template(ChangeEmailViewTemplate),

            initialize: function () {
                _changeEmailView = this;
                _changeEmailView.killTimer();
                this.model.on('change:email', function (model, value){
                    _changeEmailView.render();
                });
            },

            events:{
                'click #saveBtn':'onSaveButtonClick',
                'click #getMailCaptcha':'onGetCaptchaClick'
            },

            checkNewMail: function () {
                var newEmail = $.trim($('#newEmail').val());
                var captcha = $.trim($('#mailCaptcha').val());

                var errMsg = "";
                this.$('.change-email-body .errorMsg').html('');

                if(newEmail.length === 0){
                    //如果新邮箱为空
                    errMsg = "*新邮箱不能为空";
                }
                else if(!RegUtil.emailReg.test(newEmail)){
                    //新邮箱格式错误
                    errMsg = "*邮箱格式填写错误";
                }
                else if(captcha.length === 0){
                    //验证码为空
                    errMsg = "*验证码不能为空";
                }

                if(errMsg.length > 0){
                    this.$('.change-email-body .errorMsg').css('color','red');
                    this.$('.change-email-body .errorMsg').html(errMsg);
                    return false;
                }

                return true;
            },

            onSaveButtonClick: function () {
                if(this.checkNewMail()){
                    //发送请求修改邮箱

                    //成功后
                    this.$el.modal('hide');
                    this.model.set('email', $('#newEmail').val());

                    //清空表单
                    $('#newEmail').val('');
                    $('#mailCaptcha').val('');
                }
            },

            onGetCaptchaClick: function () {
                var newEmail = $.trim($('#newEmail').val());

                var errMsg = "";
                this.$('.change-email-body .errorMsg').html('');
                this.$('.change-email-body .errorMsg').css('color','red');
                if(newEmail.length === 0){
                    //如果新邮箱为空
                    errMsg = "*新邮箱不能为空";
                }
                else if(!RegUtil.emailReg.test(newEmail)){
                    //新邮箱格式错误
                    errMsg = "*邮箱格式填写错误";
                }

                if(errMsg.length > 0){
                    this.$('.change-email-body .errorMsg').html(errMsg);
                    return false;
                }else{
                    this.$('.change-email-body .errorMsg').css('color','#4d4d4d');
                    this.$('.change-email-body .errorMsg').html('验证码已发送至邮箱:'+ newEmail);

                    //发送验证码请求,设置获取倒计时
                    var second = 30;

                    $('#getMailCaptcha').hide();
                    $('#resetMailCaptcha').val(second + '秒后重获取');
                    $('#resetMailCaptcha').show();

                    window.getEmailCaptchaTimer = setInterval(function () {
                        second -= 1;
                        if (second > 0) {
                            $('#resetMailCaptcha').val(second + '秒后重获取');
                        } else {
                            clearInterval(getEmailCaptchaTimer);
                            $('#getMailCaptcha').show();
                            $('#resetMailCaptcha').hide();
                        }
                    }, 1000);
                }
            },

            killTimer: function () {
                if(window.getEmailCaptchaTimer != undefined || window.getEmailCaptchaTimer != null){
                    clearInterval(getEmailCaptchaTimer);
                }
            },

            render: function () {
                this.$el.html(this.template(this.model.attributes));

                return this;
            }

        });

        return changeEmailView;
});
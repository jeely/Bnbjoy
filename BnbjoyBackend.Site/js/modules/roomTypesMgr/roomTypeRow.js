/**
 * Created by lijizhuang on 16/9/19.
 */
define(['jquery', 'underscore', 'backbone', 'common', 'url', 'text!modules/roomTypesMgr/roomTypeRowView.html',  'modules/roomTypesMgr/modal/updateRoomType', 'bootstrap-dialog'],
    function ($, _, Backbone, Common, Url, RoomTypeRowViewTemplate, UpdateRoomTypeView, BootstrapDialog) {
        var roomTypeRowView = Backbone.View.extend({
            tagName: 'tr',
            className: 'infoDetail',
            template: _.template(RoomTypeRowViewTemplate),

            initialize: function (attrs) {
                _roomTypeRowView = this;
                this.options = attrs;
            },

            events: {
                'click .goUp':'onGoUpClick',
                'click .goDown':'onGoDownClick',
                'click .notes span:last-child a': 'onChangeNotesClick',
                'click .operation span:first-child a':'onUpdateClick',
                'click .operation span:last-child a':'onDeleteClick'
            },

            onGoUpClick: function (e) {
                var $parent = $(e.currentTarget).parent();
                var roomTypeId = $parent.data('roomtypeid');
                var targetRoomType = this.findRoomTypeById(roomTypeId);

                //ajax发送请求调整目标房型的排位
                var postModel = {};
                postModel.currentRank = targetRoomType.get('rank');
                postModel.bnbId = targetRoomType.get('bnbId');
                Common.sendAjaxRequest(Url.moveUpRoomTypeRank, postModel, _.bind(function (data) {
                    if (data.Content && data.Content.length > 0) {
                        //如果修改成功，内存对应的模型也要修改并同步到ui
                        var previousRoomType = this.collection.find(function (roomType) {
                            return roomType.get('sort') == targetRoomType.get('sort') - 1;
                        });

                        var currentSort = targetRoomType.get('sort');
                        targetRoomType.set('sort', previousRoomType.get('sort'));
                        previousRoomType.set('sort', currentSort);

                        var currentRank = targetRoomType.get('rank');
                        targetRoomType.set('rank', previousRoomType.get('rank'));
                        previousRoomType.set('rank', currentRank);
                    }
                }, this), _.bind(function (data) {
                    if (data.Content && data.Content.length > 0) {
                        BootstrapDialog.show({
                            title: '系统提示',
                            message: data.Content
                        });
                    }
                }, this), false);
                
            },

            onGoDownClick: function (e) {
                var $parent = $(e.currentTarget).parent();
                var roomTypeId = $parent.data('roomtypeid');
                var targetRoomType = this.findRoomTypeById(roomTypeId);

                //ajax发送请求调整目标房型的排位

                var postModel = {};
                postModel.currentRank = targetRoomType.get('rank');
                postModel.bnbId = targetRoomType.get('bnbId');
                Common.sendAjaxRequest(Url.moveDownRoomTypeRank, postModel, _.bind(function (data) {
                    if (data.Content && data.Content.length > 0) {
                        //如果修改成功，内存对应的模型也要修改并同步到ui
                        var behindRoomType = this.collection.find(function (roomType) {
                            return roomType.get('sort') == targetRoomType.get('sort') + 1;
                        });

                        var currentRank = targetRoomType.get('sort');
                        targetRoomType.set('sort', behindRoomType.get('sort'));
                        behindRoomType.set('sort', currentRank);

                        var currentRank = targetRoomType.get('rank');
                        targetRoomType.set('rank', behindRoomType.get('rank'));
                        behindRoomType.set('rank', currentRank);
                    }
                },this), _.bind(function (data) {
                    if (data.Content && data.Content.length > 0) {
                        BootstrapDialog.show({
                            title: '系统提示',
                            message: data.Content
                        });
                    }
                }, this), false);
               
            },

            onUpdateClick: function (e) {
                var $parent = $(e.currentTarget).parent();
                var roomTypeId = $parent.data('roomtypeid');
                var targetRoomType = this.findRoomTypeById(roomTypeId);

                var updateRoomTypeView = new UpdateRoomTypeView({model: targetRoomType});
                if ($('body').find('#updateRoomType').length === 0) {
                    $('body').append(updateRoomTypeView.render().el);
                }
                else {
                    $('#updateRoomType').replaceWith(updateRoomTypeView.render().el);
                }

                $("#updateRoomType").modal({backdrop: 'static', keyboard: false});
            },

            onDeleteClick: function (e) {
                var $parent = $(e.currentTarget).parent();
                var roomTypeId = $parent.data('roomtypeid');
                var targetRoomType = this.findRoomTypeById(roomTypeId);

                BootstrapDialog.confirm('将永久删除房型"'+ targetRoomType.get('roomTypeName') +'",是否确认?', function(result){
                    if(result) {
                        //ajax请求删除该房型
                        _roomTypeRowView.collection.remove(targetRoomType);
                    }
                    else{
                       //消框
                    }
                });

            },

            onChangeNotesClick: function (e) {

            },

            findRoomTypeById: function (typeId) {
                var targetRoomType = this.collection.find(function (roomType) {
                    return roomType.get('roomTypeId') == typeId;
                });

                return targetRoomType;
            },

            render: function () {
                this.$el.html(this.template($.extend({}, this.model.attributes, {peers:this.options.collection.length})));

                return this;
            }
        });

        return roomTypeRowView;
    });
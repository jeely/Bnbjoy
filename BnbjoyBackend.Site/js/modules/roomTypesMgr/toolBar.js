/**
 * Created by lijizhuang on 16/9/22.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/roomTypesMgr/toolBarView.html', 'models/roomTypesMgr/dailyPrices', 'bootstrap-dialog', 'plugins/jquery.showLoading'],
    function ($, _, Backbone, ToolBarViewTemplate, DailyPrices, BootstrapDialog) {
        var toolBarView = Backbone.View.extend({
            tagName: 'div',
            //className: 'toolbar',
            template: _.template(ToolBarViewTemplate),

            initialize: function (attrs) {
                this.options = attrs;

                var yearArr = [], monthArr1 = [], monthArr2 = []; //默认显示最近6个月, monthArr1今年的月份, monthArr2明年的月份
                var hasNextYear = false;
                var cDate = new Date();
                var cYear = cDate.getFullYear();
                var cMonth = cDate.getMonth() + 1;
                for (var i = 0; i < 6; i++) {
                    if (cMonth > 12) {
                        hasNextYear = true; //超过12月将显示明年的
                        monthArr2.push(cMonth - 12);
                    }
                    else {
                        monthArr1.push(cMonth);
                    }
                    cMonth++;
                }

                yearArr.push(cYear);
                if (hasNextYear) {
                    yearArr.push(cYear + 1);
                }

                toRenderModel = {
                    yearArr: yearArr,
                    monthArr1: monthArr1,
                    monthArr2: monthArr2,
                    collection:this.collection
                };

            },

            events: {
                'change .chooseYear select': 'onYearSelectionChange',
                'click #queryBtn':'onQueryButtonClick',
            },

            onYearSelectionChange: function (e) {
                if ($(e.currentTarget).val() == toRenderModel.yearArr[0]) {
                    $('.chooseMonth select').html('');
                    _.each(toRenderModel.monthArr1, function (month) {
                        $('.chooseMonth select').append(' <option value='+ month+ '>'+ month +'</option>');
                    });
                }
                else{
                    $('.chooseMonth select').html('');
                    _.each(toRenderModel.monthArr2, function(month){
                        $('.chooseMonth select').append(' <option value='+ month+ '>'+ month +'</option>');
                    });
                }
            },

            onQueryButtonClick: function (e) {
                 //$('#loading').showLoading();

                 var year = $('.chooseYear select').val();
                 var month = $('.chooseMonth select').val();
                 
                 var roomTypeId = $('.chooseRoomType select').val();

                 this.options.dailyPrices.reset();
                 //查询对应年月的daily price
                 var date = new Date((year + '-' + month + '-01').replace(/-/g, "/")).yyyyMMdd();
                 this.options.dailyPrices.syncDailyPrice(date, roomTypeId, function (data) {
                     //成功的回调
                     //$('#loading').hideLoading();
                 }, function (data) {
                     //失败的回调
                     //$('#loading').hideLoading();
                 });
            },

            render: function () {
                this.$el.html(this.template(toRenderModel));
                return this;
            }
        });

        return toolBarView;
    });
﻿/**
 * Created by lijizhuang on 16/9/13.
 */
require(['require.config'], function (Config) {
    require(['models/registerAndLogin/loginStatus', 'modules/registerAndLogin/register'], function (LoginStatus, RegisterView) {
        var loginStatus = new LoginStatus();
        var registerView = new RegisterView({ model: loginStatus });
        $('body').append(registerView.render().el);
    });
});
﻿using Bnbjoy.Business.Common;
using Bnbjoy.Business.Model.RoomType;
using BnbjoyBackend.Site.ConstantsNS;
using BnbjoyBackend.Site.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BnbjoyBackend.Site.Controllers
{
    [BnbjoyAuthorize]
    public class RoomTypeController : BaseController
    {
        // GET: RoomType
        public ActionResult Index()
        {
            //跳转至RoomType/Catalog
            return RedirectToAction("Catalog", "RoomType");
        }

        public ActionResult Catalog() 
        {
            ViewBag.Title = "宿说掌柜-房型设置";
            this.ViewBag.Nav = Constants.RoomInfoManagement;
            this.ViewBag.SubNav = Constants.RoomTypeManagement;
            return View();
        }

        public ActionResult DailyPrice() 
        {
            ViewBag.Title = "宿说掌柜-房价设置";
            this.ViewBag.Nav = Constants.RoomInfoManagement;
            this.ViewBag.SubNav = Constants.RoomPriceManagement;
            return View();
        }

        /// <summary>
        /// 获取当前的Bnb信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CurrentBnb() 
        {
           dynamic currentBnb = Session[Constants.OnSelectBnbSession] as dynamic;

           if (currentBnb != null)
           {
               string bnbJson = JsonHelper.ConvertObjectToJson(currentBnb);
               return Json(new HttpPostResponse(HttpStatusCode.OK, bnbJson));
           }
           else 
           {
               return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "获取session中的Bnb信息失败"));
           }
            
        }

        /// <summary>
        /// 新建房型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> NewRoomType(NewRoomTypeModel model)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string roomTypeJson = JsonConvert.SerializeObject(model);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "roomtype/new_room_type", roomTypeJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的注册请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，注册失败"));
            }
        }

        /// <summary>
        /// 获取房型列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> FetchRoomTypes(QueryAllRoomTypesParam param) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string requestJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "roomtype/catalog", requestJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的获取请求"));
                }
            }
            catch (Exception ex) 
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，获取房型列表失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> MoveUpRoomTypeRank(ExchangeRankParam param) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string requestJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "roomtype/move_up_rank", requestJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的修改请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，修改房型顺序失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> MoveDownRoomTypeRank(ExchangeRankParam param) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string requestJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "roomtype/move_down_rank", requestJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的修改请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，修改房型顺序失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> ModifyDailyPrice(UpdateDailyPriceModel model) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string requestJson = JsonConvert.SerializeObject(model);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "roomtype/update_daily_price", requestJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的修改请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，修改房价失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> ListDailyPrice(ListDailyPriceParam param) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string requestJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "roomtype/list_daily_price", requestJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的获取请求"));
                }
            }
            catch (Exception ex) 
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，获取房价失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> ListCustomDailyPrice(ListCustomDailyPriceParam param)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string requestJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "roomtype/list_custom_daily_price", requestJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的获取请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，获取房价失败"));
            }
        }

    }
}
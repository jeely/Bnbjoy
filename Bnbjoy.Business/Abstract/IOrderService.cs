﻿using Bnbjoy.Business.Constants;
using Bnbjoy.Business.Model.Dashboard;
using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Abstract
{
    public interface IOrderService : IDisposable
    {
        Task<OrderResponse> NewOrder(OrderType orderType, Order order, IEnumerable<Customer> customers, IEnumerable<OrderSpending> spendings);

        Task<string> FetchMobileArea(string mobileNumber);

        Task<QueryAvailableRoomsResponse> FetchAvailableRooms(QueryAvailableRoomsParam param);
    }
}

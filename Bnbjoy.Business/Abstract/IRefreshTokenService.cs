﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Abstract
{
    public interface IRefreshTokenService : IDisposable
    {
        Task<RefreshToken> Get(string Id);

        Task<bool> Save(RefreshToken refreshToken);

        Task<bool> Remove(string Id);
    }
}

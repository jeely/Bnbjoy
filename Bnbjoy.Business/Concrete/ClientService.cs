﻿using Bnbjoy.Business.Abstract;
using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Concrete
{
    public class ClientService : IClientService
    {
        private IClientRepository _clientRepository;

        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task<Client> FindClient(string Id)
        {
            var client = await _clientRepository.FindById(Id);
            return client;
        }

        public async Task<bool> HasClient()
        {
            return await _clientRepository.FirstClient() != null;
        }

        public async Task<Client> AddClient(Client client)
        {
            return await _clientRepository.InsertClient(client);
        }

        public async Task<IEnumerable<Client>> AddClients(IEnumerable<Client> clients)
        {
            return await _clientRepository.InsertClients(clients);
        }

        public void Dispose()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Common
{
    public static class CommonUtil
    {
        public static int ObtainValueFromEnumStr(Type enumType, string enumStr) 
        {
            try
            {
                int value = (int)Enum.Parse(enumType, enumStr);
                return value;
            }
            catch (Exception ex) 
            {
                return -1; //转换出异常，直接返回-1
            }
        }
        
    }
}

﻿using Bnbjoy.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Common
{
    public static class BusinessUtil
    {
        /// <summary>
        /// 根据订单类型生成订单号
        /// </summary>
        /// <param name="orderType"></param>
        /// <returns></returns>
        public static string GenOrderId(OrderType orderType)
        {
            string prefixCode = string.Empty;
            switch (orderType)
            {
                case OrderType.BO: prefixCode = "B"; break;
                case OrderType.AO: prefixCode = "A"; break;
                case OrderType.PO: prefixCode = "P"; break;
            }

            prefixCode += DateTime.Now.Year.ToString();
            prefixCode += DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            prefixCode += DateTime.Now.Day.ToString().Length == 1 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            prefixCode += DateTime.Now.Hour.ToString().Length == 1 ? "0" + DateTime.Now.Hour.ToString() : DateTime.Now.Hour.ToString();
            prefixCode += DateTime.Now.Minute.ToString().Length == 1 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString();
            prefixCode += DateTime.Now.Second.ToString().Length == 1 ? "0" + DateTime.Now.Second.ToString() : DateTime.Now.Second.ToString();
            if (DateTime.Now.Millisecond.ToString().Length == 1)
            {
                prefixCode += "00" + DateTime.Now.Millisecond.ToString();
            }
            else if (DateTime.Now.Millisecond.ToString().Length == 2)
            {
                prefixCode += "0" + DateTime.Now.Millisecond.ToString();
            }
            else
            {
                prefixCode += DateTime.Now.Millisecond.ToString();
            }

            return prefixCode;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Constants
{
    public enum OrderStatus
    {
        UnConfirmation = 1, //未确认
        Confirmation = 2,  //已确认
        Processing = 3, //处理中
        Completed = 4,  //已结束
        Canceled = 5    //已取消
    }
}

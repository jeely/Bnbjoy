﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Constants
{
    public enum RoomStatus
    {
        Bookable = 1,  //可预订
        Booked,         //已预订
        CheckedIn,      //已入住
        Away,           //已离开
        Locked,         //已关闭
        MakeUp          //可补录
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class NewRoomTypeModel
    {
        [JsonProperty("roomTypeName")]
        [Required(ErrorMessage = "房型名称不能为空")]
        [StringLength(20, ErrorMessage = "房型名不超过20个字符")]
        public string RoomTypeName { get; set; }

        [JsonProperty("notes")]
        [StringLength(4, ErrorMessage = "房型描述不超50个字符")]
        public string Notes { get; set; }

        [JsonProperty("bnbId")]
        [Required(ErrorMessage = "BnbId不能为空")]
        public string BnbId { get; set; }

        [JsonProperty("rooms")]
        [Required(ErrorMessage = "房间列表不能为空")]
        public IEnumerable<NewRoomModel> NewRoomList { get; set; }

        [JsonProperty("commonPrice")]
        [Required(ErrorMessage = "默认价格不能为空")]
        public NewCommonPriceModel CommonPrice { get; set; }
    }
}

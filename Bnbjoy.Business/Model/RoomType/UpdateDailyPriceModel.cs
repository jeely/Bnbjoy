﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class UpdateDailyPriceModel
    {
        [JsonProperty("fromDate")]
        [Required(ErrorMessage = "起始日期不能为空")]
        public string FromDate { get; set; }

        [JsonProperty("toDate")]
        [Required(ErrorMessage = "截止日期不能为空")]
        public string ToDate { get; set; }

        [JsonProperty("roomTypeId")]
        [Required(ErrorMessage="房型Id不能为空")]
        public string RoomTypeId { get; set; }

        [JsonProperty("price")]
        [Required(ErrorMessage="房价不能为空")]
        public decimal Price { get; set; }
    }
}

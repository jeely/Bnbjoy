﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class QueryAllRoomTypesParam
    {
        [JsonProperty("bnbId")]
        [Required(ErrorMessage = "BnbId不能为空")]
        public string BnbId { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class ListDailyPriceResponse
    {
        [JsonProperty("dateTime")]
        public string DateTime { get; set; }

        [JsonProperty("priceArray")]
        public decimal PriceArray { get; set; }
    }
}

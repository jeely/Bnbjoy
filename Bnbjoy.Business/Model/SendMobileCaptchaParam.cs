﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model
{
    public class SendMobileCaptchaParam
    {
        [JsonProperty("mobileNumber")]
        [Required(ErrorMessage = "手机号不能为空")]
        [RegularExpression(@"^1[3|4|5|7|8]\d{9}$", ErrorMessage = "手机号格式错误")]
        public string MobileNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class QueryAvailableRoomsResponse
    {
        public int Total { get; set; }

        public IEnumerable<DateRoomsPair> Pairs { get; set; }
    }

    public class DateRoomsPair
    {
        public string Date { get; set; }

        public int RoomCount { get; set; }
    }
}

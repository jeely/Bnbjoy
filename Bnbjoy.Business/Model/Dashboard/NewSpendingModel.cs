﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class NewSpendingModel
    {
        [JsonProperty("spendType")]
        [Required(ErrorMessage = "消费类别不能为空")]
        public string SpendType { get; set; } //字符串表示js传来的枚举

        [JsonProperty("amount")]
        [Required(ErrorMessage = "消费金额不能为空")]
        [Range(typeof(Decimal), "1", "10000000", ErrorMessage = "价格范围应为1->10000000")] 
        public decimal Amount { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class HeadInfoParam
    {
        [JsonProperty("userId")]
        [Required(ErrorMessage = "用户Id不能为空")]
        public string UserId { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class QueryRoomsBasiceInfoParam
    {
        [JsonProperty("bnbId")]
        [Required(ErrorMessage = "bnb Id不能为空")]
        public string BnbId { get; set; }

        [JsonProperty("fromDate")]
        [Required(ErrorMessage = "起始时间不能为空")]
        public DateTime FromDate { get; set; }

        [JsonProperty("toDate")]
        [Required(ErrorMessage = "截止时间不能为空")]
        public DateTime ToDate { get; set; }
    }
}

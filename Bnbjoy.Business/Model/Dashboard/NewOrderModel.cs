﻿using Bnbjoy.Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class NewOrderModel
    {
        [JsonProperty("orderType")]
        [Required(ErrorMessage = "订单类型不能为空")]
        public string OrderType { get; set; }

        [JsonProperty("paymentType")]
        [Required(ErrorMessage = "付款方式不能为空")]
        public string PaymentType { get; set; }

        [JsonProperty("fromDate")]
        [Required(ErrorMessage = "入住时间不能为空")]
        public System.DateTime FromDate { get; set; }

        [JsonProperty("toDate")]
        [Required(ErrorMessage = "离店时间不能为空")]
        public System.DateTime ToDate { get; set; }

        [JsonProperty("roomNights")]
        [Required(ErrorMessage = "间夜数不能为空")]
        public int RoomNights { get; set; }

        [JsonProperty("channel")]
        [Required(ErrorMessage = "渠道不能为空")]
        public string Channel { get; set; } //客户端过来的枚举类型用字符串表示

        [JsonProperty("roomId")]
        [Required(ErrorMessage = "房间Id不能为空")]
        public string RoomId { get; set; }

        [JsonProperty("roomNumber")]
        [Required(ErrorMessage = "房号不能为空")]
        public string RoomNumber { get; set; }

        [JsonProperty("reserverUser")]
        [Required(ErrorMessage = "预订人姓名不能为空")]
        public string ReserverUser { get; set; }

        [JsonProperty("reserverMobile")]
        [Required(ErrorMessage = "预订人手机号不能为空")]
        public string ReserverMobile { get; set; }

        [JsonProperty("amount")]
        [Required(ErrorMessage = "收款金额不能为空")]
        public decimal Amount { get; set; }

        [JsonProperty("arrivalTime")]
        [Required(ErrorMessage = "最晚到达时间不能为空")]
        public string ArrivalTime { get; set; }

        [JsonProperty("bnbId")]
        [Required(ErrorMessage = "bnb Id不能为空")]
        public string BnbId { get; set; }

        [JsonProperty("roomTypeId")]
        [Required(ErrorMessage = "房型Id不能为空")]
        public string RoomTypeId { get; set; }

        [JsonProperty("orderNotes")]
        public string OrderNotes { get; set; }

        [JsonProperty("customers")]
        [Required(ErrorMessage = "入住人信息不能为空")]
        public IEnumerable<NewCustomerModel> Customers { get; set; }

        [JsonProperty("spendings")]
        [Required(ErrorMessage = "消费明细不能为空")]
        public IEnumerable<NewSpendingModel> Spendings { get; set; }
    }
}

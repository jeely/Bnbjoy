﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class RoomsBasicInfoModel
    {
        public string RoomId { get; set; }

        public string RoomNumber { get; set; }

        public string RoomTypeId { get; set; }

        public string RoomTypeName { get; set; }

        public SortedDictionary<string, decimal> DailyPrice { get; set; }

        public IEnumerable<BasicOrderModel> OrderModels { get; set; }

    }

    public class BasicOrderModel 
    {
        public string OrderId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int RoomStatus { get; set; }

        public string ReserverUser { get; set; }

        public string RoomId { get; set; }
    }
}
